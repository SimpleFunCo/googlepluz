package com.gmail.simplefunco.googlepluz.controller.callback;
/* Created on 3/28/2017. */

public interface OnListFragmentInteractionListener {

    void onListFragmentInteraction(long personId);
}
