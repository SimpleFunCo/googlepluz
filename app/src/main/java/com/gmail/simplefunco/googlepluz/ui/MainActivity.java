package com.gmail.simplefunco.googlepluz.ui;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.gmail.simplefunco.googlepluz.R;
import com.gmail.simplefunco.googlepluz.controller.callback.OnListFragmentInteractionListener;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.Plus;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleBrowserClientRequestUrl;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.people.v1.People;
import com.squareup.picasso.Picasso;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends FragmentActivity implements
        OnListFragmentInteractionListener,
        GoogleApiClient.OnConnectionFailedListener {

    private GoogleApiClient mGoogleApiClient;
    private static final int REQ_CODE = 9001;

    @BindView(R.id.nvMenu)
    NavigationView nvLeftDrawer;
    @BindView(R.id.dlMainLeft)
    DrawerLayout dlMainLeft;
    @BindView(R.id.ndBottomLine)
    View ndBottomLine;
    @BindView(R.id.tvSignOut)
    TextView tvSignOut;
    @BindView(R.id.btnSignIn)
    Button btnSignIn;

    CircleImageView navDrawProfilePicture;
    TextView ndUserName;
    TextView ndUserEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);

        ButterKnife.bind(this);

        View nvHeader = nvLeftDrawer.getHeaderView(0);
        navDrawProfilePicture = (CircleImageView) nvHeader.findViewById(R.id.ndProfilePicture);
        ndUserName = (TextView) nvHeader.findViewById(R.id.ndUserName);
        ndUserEmail = (TextView) nvHeader.findViewById(R.id.ndUserEmail);

        

        dlMainLeft.openDrawer(GravityCompat.START);
        dlMainLeft.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_OPEN);
        nvLeftDrawer.getMenu().setGroupVisible(R.id.ndMenuMainLeft, false);
        ndBottomLine.setVisibility(View.INVISIBLE);
        tvSignOut.setVisibility(View.INVISIBLE);

        nvLeftDrawer.setNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.ndContacts:
                    getContacts();
                    break;
                case R.id.ndInfo:

                    break;
            }
            return true;
        });

        btnSignIn.setOnClickListener(item -> signIn());
        tvSignOut.setOnClickListener(item -> signOut());

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.flMainHost, PeopleFragment.newInstance(5, new ArrayList<Object>())).commit();

        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions
                .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions)
                .build();
    }

    private void getContacts() {

    }

    @Override
    public void onListFragmentInteraction(long personId) {

    }

    private void signIn() {
        Intent intent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(intent, REQ_CODE);
    }

    private void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                updateUi(false);
            }
        });
    }

    private void handleResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            GoogleSignInAccount account = result.getSignInAccount();
            ndUserName.setText(account.getDisplayName());
            ndUserEmail.setText(account.getEmail());
            Picasso.with(getApplicationContext()).load(account.getPhotoUrl().toString()).into(navDrawProfilePicture);
            updateUi(true);
        } else {
            updateUi(false);
        }
    }

    private void updateUi(boolean loggedIn) {
        if (loggedIn) {
            navDrawProfilePicture.setVisibility(View.VISIBLE);
            ndUserName.setVisibility(View.VISIBLE);
            ndUserEmail.setVisibility(View.VISIBLE);
            dlMainLeft.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            nvLeftDrawer.getMenu().setGroupVisible(R.id.ndMenuMainLeft, true);
            ndBottomLine.setVisibility(View.VISIBLE);
            tvSignOut.setVisibility(View.VISIBLE);

            btnSignIn.setVisibility(View.INVISIBLE);
        } else {
            navDrawProfilePicture.setVisibility(View.INVISIBLE);
            ndUserName.setVisibility(View.INVISIBLE);
            ndUserEmail.setVisibility(View.INVISIBLE);
            dlMainLeft.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_OPEN);
            nvLeftDrawer.getMenu().setGroupVisible(R.id.ndMenuMainLeft, false);
            ndBottomLine.setVisibility(View.INVISIBLE);
            tvSignOut.setVisibility(View.INVISIBLE);

            btnSignIn.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQ_CODE) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleResult(result);
        }
    }



}
