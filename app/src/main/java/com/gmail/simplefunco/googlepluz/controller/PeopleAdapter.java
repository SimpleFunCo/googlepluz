package com.gmail.simplefunco.googlepluz.controller;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.gmail.simplefunco.googlepluz.R;

import com.gmail.simplefunco.googlepluz.controller.callback.OnListFragmentInteractionListener;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import lombok.Getter;
import lombok.Setter;

public class PeopleAdapter extends RecyclerView.Adapter<PeopleAdapter.ViewHolder> {

    private final List<?> mValues;
    private final OnListFragmentInteractionListener mListener;
    private Context mContext;

    public PeopleAdapter(Context mContext, List<?> mValues, OnListFragmentInteractionListener listener) {
        this.mContext = mContext;
        this.mValues = mValues;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.people_row_item, parent, false);
        return new ViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Picasso.with(mContext).load("some url").into(holder.civProfilePicture);
        holder.tvFirstName.setText("Sometext");
        holder.tvLastName.setText("Sometext");
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.rowProfilePicture)
        CircleImageView civProfilePicture;
        @BindView(R.id.tvFirstName)
        TextView tvFirstName;
        @BindView(R.id.tvLastName)
        TextView tvLastName;
        @BindView(R.id.ibInfo)
        ImageButton ibInfo;
        @Getter @Setter
        private long personId;
        private OnListFragmentInteractionListener itemClickListener;

        public ViewHolder(View view, OnListFragmentInteractionListener itemClickListener) {
            super(view);
            ButterKnife.bind(this, view);
            this.itemClickListener = itemClickListener;
            ibInfo.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.ibInfo) {
                itemClickListener.onListFragmentInteraction(personId);
            }
        }
    }
}
